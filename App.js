/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {Provider} from 'react-redux';
import AppIndex from './src/router/index';
import Store from './src/redux/store';

const App = () => {
  return (
    <Provider store={Store}>
        <AppIndex/>
    </Provider>
  );
};


export default App;
