# Sample News App

## App preview
![Home screen](screenshots/feed.png "Home screen")
![News Screen](screenshots/post.png "News Screen")
![Comments](screenshots/comments.png "Comments")
![Create comment](screenshots/new_comment.png "Create Comment")
![Update comment](screenshots/update_comment.png "Update comment Screen")
![Create News Screen](screenshots/create_news.png "Create news Screen")

## App Features/Functionality
- Home page showing news list
- Infinte Scrolling
- Refresh news list (Pull list down to trigger refresh)
- View Single News Page
- View Comments
- Post Comments
- Edit Comment
- Create News Form
- Update News Form


## Technical stack
- react-navigation
- react-redux
- react-native-elements
- axios
- redux-thunk
- redux


## Getting Started

These instructions below will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

You need to have the following:

```
Xcode
NPM
Node
A laptop :(
```

### Installing

Clone Project. Run:
```
git clone https://methodmain@bitbucket.org/methodmain/news-app.git
```

Install the packages

```
yarn install
```

Also, install necessary pods

```
pod install
```

## Usage
- [Setting up React Native](https://facebook.github.io/react-native/docs/getting-started.html)
- [Running app on Android Device](https://facebook.github.io/react-native/docs/running-on-device-android.html)
- [Running app on iOS Device](https://facebook.github.io/react-native/docs/running-on-device-ios.html)
To run NewsApp on iOS you need to open `ios/NewsApp.xcworkspace` instead of `ios/NewsApp.xcodeproj` in Xcode.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
