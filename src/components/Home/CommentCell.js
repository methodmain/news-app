import React from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';

const CommentCell = props => {
    return(
        <TouchableWithoutFeedback>
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <View>
                    <Image
                        style={{ width: 50, height:50, borderRadius: 25, marginTop:0 }}
                        source={{uri: props.item.avatar}}
                    />
                </View>
                <View style={{ paddingLeft: 10, marginRight: 10, width: '100%' }}>
                    <View><Text style={styles.authorText} >{props.item.name}</Text></View>
                    <View><Text style={styles.date}>{props.item.createdAt}</Text></View>
                    <View><Text style={styles.contentText} >{props.item.comment}</Text></View>
                </View>
                
            </View>
            <View style={{ flexDirection: 'row', paddingBottom: 10, justifyContent: 'flex-end' }}>
                <TouchableOpacity onPress={() => props.editAction()}>
                    <Text style={{ color: '#00f', fontWeight: '100', fontSize: 12 }} >Edit </Text>
                </TouchableOpacity>
                <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 12 }}> | </Text>
                <TouchableOpacity>
                    <Text style={{ color: '#f00', fontWeight: '100', fontSize: 12 }}> Delete</Text>
                </TouchableOpacity>
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        width: '100%'
    }, 
    subContainer: { 
        paddingTop: 10,
        flexDirection: 'row',
        width: '100%'
    },
    date: {
        fontSize: 10, 
        fontWeight: '100', 
        color: '#aaa'
    },
    contentText: {
        fontWeight: '200', 
        color: '#000'
    },
    authorText: {
        fontWeight: '400', 
        color: '#000'
    }
});

export default CommentCell;