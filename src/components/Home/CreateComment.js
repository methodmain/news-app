import React, { Component } from 'react';
import { View, ActivityIndicator, TextInput, Modal, Text, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import * as colors from '../Utils/Colors';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class CreateComment extends Component {

  state = {
    modalVisible: false,
  };

  static getDerivedStateFromProps(props, state) {
    return { 
        modalVisible: props.isVisbile 
    };
  }


  //sets defaultProps for close modal here
  render() {
      return (
        <Modal
          animationType={'fade'}
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => {}}
        >
        <View
        style={styles.containerStyle}
        >
        <View
          style={styles.innerStyle}
        >
          <View style={{ marginBottom: 10, width: '100%', paddingLeft: 20, alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 20, color: '#000' }}>Leave a comment...</Text>
          </View>

          <View style={styles.textInputContainer}>
            <TextInput
              placeholder="Your name"
              underlineColorAndroid="transparent"
              placeholderTextColor="grey"
              style={styles.textInput}
              onChangeText={value => this.props.getInput({ prop: 'name', value })}
              value={this.props.name}
            />
          </View>

          <View style={styles.textAreaContainer} >
            <TextInput
              style={styles.textArea}
              underlineColorAndroid="transparent"
              placeholder="Comment..."
              placeholderTextColor="grey"
              numberOfLines={10}
              multiline
              onChangeText={value => this.props.getInput({ prop: 'comment', value })}
              value={this.props.comment}
            />
          </View>

          <Button
            title='Post'
            backgroundColor={colors.APP_GREEN}
            buttonStyle={{ minWidth: '90%', marginBottom: 10, marginTop: 10, backgroundColor: colors.APP_GREEN }}
            borderRadius={4}
            onPress={this.props.okButtonAction}
            disabled={this.props.loading}
            loading={this.props.loading}
          />

        <Button
            type="clear"
            buttonStyle={{ minWidth: '100%', marginBottom: 10 }}
            borderRadius={4} 
            title='Cancel'
            titleStyle={{ fontSize: 13, color: colors.APP_RED }}
            onPress={this.props.onDismiss}
        />
        </View>
        </View>
        </Modal>
      );
  }
}

const styles = {
  containerStyle: {
    backgroundColor: 'rgba(0,0,0,.25)',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderColor: 'rgba(0, 0, 0, 1)',
  },
  innerStyle: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: '#ffffff',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    paddingTop: 20
  },
  textAreaContainer: {
    borderColor: '#eee',
    borderWidth: 1,
    width: '90%',
   //marginLeft: 20,
    //marginRight: 20
  },
  textArea: {
    height: 100,
    width: '100%',
    margin: 1,
    textAlignVertical: 'top',
    paddingLeft: 10,
    paddingRight: 10
  },
  textInputContainer: {
    borderColor: '#eee',
    borderWidth: 1,
    marginBottom: 10,
    width: '90%',
    //marginLeft: 20,
    //marginRight: 20
  },
  textInput: {
    width: '100%',
    margin: 1,
    height: 40,
    paddingLeft: 10,
    paddingRight: 10
  }
};
