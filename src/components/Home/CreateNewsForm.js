import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import  {withNavigation} from 'react-navigation';
import { Input, Button } from 'react-native-elements';
import Header from '../Utils/Header';
import * as colors from '../Utils/Colors';

class CreateNewsForm extends React.Component {

    render() {
        return(
            <View style={{ backgroundColor: colors.THEME_COLOR }}>
                <SafeAreaView>
                    <Header title='Create News' showBackButton backAction={() => this.props.navigation.navigate('welcome')}/>
                    <ScrollView style={{ backgroundColor: colors.APP_WHITE, padding: 20 }}>
                        <View style={styles.captionContainer}>
                            <Text style={styles.captionText}>Let your friends and community know what is happening around them.</Text>
                        </View>

                        <View style={{ paddingTop: 10 }}>
                            <Input inputStyle={styles.input} placeholder="Author" />
                        </View>

                        <Input inputStyle={styles.input} placeholder="Title" />

                        <TextInput
                           style={{ ...styles.textAreaInput, padding: 15 }}
                           multiline
                           numberOfLines={6}
                           placeholder="Content..."
                        />

                        <View style={styles.buttonContainer}>
                            <Button buttonStyle={styles.button} titleStyle={styles.buttonTitle}  title="Create"/>
                        </View>
                    </ScrollView>
                 </SafeAreaView>
            </View>
        )
    }
}

const styles= StyleSheet.create({
        input: {
            fontSize: 12,
            lineHeight: 16,
        },
      textAreaInput:{
         borderWidth:0,
         borderBottomWidth: 0.8,
         padding:0,
         borderRadius:10,
         borderColor: 'grey',
         minHeight: 100,
         fontSize: 12,
         lineHeight: 16,
      },
      captionContainer:{
        color: '#000000',
        fontSize: 18,
        lineHeight: 25,
        fontWeight: '400'
      },
      captionText:{
        paddingRight: 20,
        color: '#707070',
        fontSize: 12,
        lineHeight: 16,
        marginTop: 5
     },
     buttonContainer: {
        marginTop: 20
     },
     button: {
         backgroundColor: colors.APP_GREEN
     },
     buttonTitle: {
        fontSize: 14,
     }
});

export default withNavigation(CreateNewsForm);