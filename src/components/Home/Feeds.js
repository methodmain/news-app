import React from 'react';
import {View, Text, StyleSheet, SafeAreaView, FlatList, ActivityIndicator} from 'react-native';
import  {withNavigation} from 'react-navigation';
import { connect } from 'react-redux';
import { fetchAllNews, refreshNewsList, loadMoreNewsList, selectNews } from '../../redux/actions';
import Header from '../Utils/Header';
import { relativeHeight } from '../Utils/Functions'
import NewsItem from './NewsItem';
import FloatingButton from './FloatingButton';
import * as colors from '../Utils/Colors';

class Feeds extends React.Component {

    static navigationOptions = () => ({
        headerVisible: true
      });

    async componentDidMount() {
        await this.props.fetchAllNews();
    }

    handleRefresh = async () => {
        await this.props.refreshNewsList();
    }

    handleLoadMore = async () => {
        await this.props.loadMoreNewsList(this.props.current_page + 1);
    }

    renderSeparator() {
        return (
            <View style={{ backgroundColor: '#fff', height: 0.5 }} />
        );
    };

    goToNews = (newsItem) => {
        this.props.selectNews(newsItem);
        this.props.navigation.navigate('news');
    }

    renderFooter() {
        //if (!this.props.updateNewsLoading) return <View style={{ height: 40 }} />;
    
        return (
          <View
            style={{
              paddingVertical: 20,
              paddingBottom: 20
            }}
          >
            <ActivityIndicator animating size="large" />
          </View>
        );
      }

    render() {
        return(
            <View style={{ flex:1, backgroundColor: colors.THEME_COLOR }}>
                <SafeAreaView>
                    <Header title='NEWS READER'/>
                    <FlatList
                        style={{ height: relativeHeight(50), backgroundColor: '#fff', paddingBottom: 50 }}
                        data={this.props.newsList}
                        renderItem={({ item, index }) => <NewsItem item={item} index={index} clickAction={() => this.goToNews(item)} />}
                        keyExtractor={(item, index)=> item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter}
                        refreshing={this.props.refreshNewsLoading}
                        onRefresh={this.handleRefresh}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0}
                    />
                    <FloatingButton buttonAction={() => this.props.navigation.navigate('create')} />
                </SafeAreaView>
            </View>
        )
    }
}

const mapStateToProps = ({ news }) => {
    const { newsList, fetchNewsLoading, fetchNewsResponse,refreshNewsLoading, current_page, updateNewsLoading } = news;
    
    return { newsList, fetchNewsLoading, fetchNewsResponse, refreshNewsLoading, current_page, updateNewsLoading };
};

const mapActionCreators = {
    fetchAllNews,
    refreshNewsList,
    loadMoreNewsList,
    selectNews
};

export default connect(mapStateToProps, mapActionCreators)(withNavigation(Feeds));