import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const FloatingButton = props => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => props.buttonAction()}>
            <View style={styles.iconContainer}>
                <Text style={styles.iconText}>+</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: { 
        position: 'absolute', 
        bottom: 50, 
        height: 50, 
        width: 50, 
        borderRadius: 25, 
        backgroundColor: '#f00',
        right: 30,
        shadowColor: '#000000',
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: { width: 0, height: 3 },
        elevation: 3,
    }, 
    iconContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconText: {
        fontSize: 40,
        fontWeight: 'bold',
        color: '#fff',
        height: '100%'
    }
});

export default FloatingButton;