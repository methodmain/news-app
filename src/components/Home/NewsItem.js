import React from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';

const NewsItem = props => {
    return(
        <TouchableWithoutFeedback onPress={() => props.clickAction()}>
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <View style={styles.imageContainer}>
                    <Text style={styles.textColor}>{props.item.title.charAt(0)}</Text>
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.date}>{props.item.createdAt}</Text>
                    <View style={styles.titleContainer}>
                        <Text style={styles.titleText}>{props.item.title}</Text>
                    </View>
                    <View style={styles.bodyContainer}>
                        <Text style={styles.bodyText} numberOfLines={3}>{props.item.body}</Text>
                    </View>
                    <View>
                        <Text style={styles.authorText}>{props.item.author}</Text>
                    </View>
                </View>
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        //height: 150,
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 40,
        marginRight: 20,
        shadowColor: '#000000',
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: { width: 0, height: 3 },
        elevation: 3,
    },
    date: {
        fontSize: 10, 
        fontWeight: '100', 
        color: '#aaa'
    },
    subContainer: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 5
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        backgroundColor: '#21be87',
        borderRadius: 5,
        marginLeft: -25,
        alignSelf: 'center'
    },
    textColor: {
        fontSize: 40,
        color: '#ffffff',
    },
    contentContainer: {
        padding: 10,
        paddingLeft: 10
    },
    titleContainer: {
        paddingRight: 20
    },
    titleText: {
        fontSize: 15,
        color: '#f00',
        fontWeight: '500',
    },
    bodyText: {
        fontSize: 10,
        color: '#000',
        flexShrink: 1,
        flexWrap: 'wrap'
    },
    bodyContainer: {
        height: 50,
        paddingRight: 25,
        paddingTop: 5
    },
    authorText: {
        fontSize: 12,
        fontWeight: '200'
    }
})

export default NewsItem;