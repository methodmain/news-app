import React from 'react';
import {View, Text, StyleSheet, SafeAreaView,ScrollView, FlatList, TouchableOpacity} from 'react-native';
import  {withNavigation} from 'react-navigation';

class Sample extends React.Component {

    render() {
        return(
            <View>
                <SafeAreaView>
                    <Text>Sample</Text>
                 </SafeAreaView>
            </View>
        )
    }
}

export default withNavigation(Sample);