import React from 'react';
import {View, Text, Image } from 'react-native';

const SingleImage = props => {
    return (
        <View style={{ height: '100%', paddingLeft: 20, paddingTop: 20, paddingBottom: 20, backgroundColor: '#fff' }}>
            <Image
                style={{ width: 300, height:300, marginTop:0 }}
                source={{uri: props.item.image}}
            />
        </View>
    )
}

export default SingleImage;