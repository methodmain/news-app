import React from 'react';
import {View, Text, StyleSheet, SafeAreaView, ScrollView, ActivityIndicator, FlatList, TouchableOpacity, TouchableWithoutFeedback, Alert } from 'react-native';
import { connect } from 'react-redux';
import {withNavigation} from 'react-navigation';
import SingleImage from './SingleImage';
import CommentCell from './CommentCell';
import Header from '../Utils/Header';
import CreateComment from './CreateComment';
import EditComment from './EditComment';
import { relativeHeight } from '../Utils/Functions'
import * as colors from '../Utils/Colors';
import { fetchAllComments, fetchAllImages, 
    getCommentInputData, postComment, resetCommentFormData,
    selectComment, editComment  
} from '../../redux/actions';

class SingleNewsItem extends React.Component {

    state = {
        isCreateCommentVisible: false,
        isEditCommentVisible: false
    };

    async componentDidMount() {
        await this.props.fetchAllImages(this.props.selectedNews.id);
        await this.props.fetchAllComments(this.props.selectedNews.id);
    }

    updateNewsAction = async () => {
        this.props.navigation.navigate('updateNews');
    }

    deleteNewsAction = async () => {

    }

    submitComment = async () => {
        const { name, comment, selectedNews } = this.props;
        
        await this.props.postComment(name, comment, selectedNews.id);
    
        if (this.props.postCommentHasError === false) {
            //refresh comment list
            await this.props.fetchAllComments(this.props.selectedNews.id);

            this.setState({ isCreateCommentVisible: false }); //closes the modal
            setTimeout(() => {
                Alert.alert('Success', 'Comment Posted!');
            }, 500);
        } else {
          const { title, message, } = this.props.postCommentResponse.errors;
          setTimeout(() => {
            Alert.alert(title, message);
          }, 500);
        }
      };

    updateAComment = async () => {
        const { name, comment, selectedNews, selectedComment } = this.props;

        await this.props.editComment(name, comment, selectedNews.id, selectedComment.id );

        if (this.props.updateCommentHasError === false) {
            //refresh comment list
            await this.props.fetchAllComments(this.props.selectedNews.id);

            this.setState({ isEditCommentVisible: false }); //closes the modal
            setTimeout(() => {
            Alert.alert('Success', 'Comment Updated!');
            }, 500);
        } else {
            const { title, message, } = this.props.updateCommentResponse.errors;
            setTimeout(() => {
            Alert.alert(title, message);
            }, 500);
        }
    };

    //action to open edit comment modal
    updateCommentAction = async (commentItem) => {
        this.props.selectComment(commentItem)
        this.setState({ isEditCommentVisible: true });
    }

    //action to delete comment
    deleteCommentAction = async () => {

    }

    //togle comment modal visibility
    toggleCreateCommentVisibility(isCreateCommentVisible) {
        this.props.resetCommentFormData();
        this.setState({ isCreateCommentVisible: !isCreateCommentVisible });
    }

    toggleUpdateCommentVisibility(isEditCommentVisible) {
        this.props.resetCommentFormData();
        this.setState({ isEditCommentVisible: !isEditCommentVisible });
    }

    renderActivityLoading() {
        return (
          <View
            style={{
              paddingVertical: 20,
              paddingBottom: 20
            }}
          >
            <ActivityIndicator animating size="large" />
          </View>
        );
      }

    renderSeparator() {
        return (
            <View style={{ backgroundColor: '#fff', width: 0 }} />
        );
    };

    renderVerticalSeparator() {
        return (
            <View style={{ backgroundColor: '#aaa', height: 0.5 }} />
        );
    };

    render() {
        return(
            <View style={{ flex:1, backgroundColor: colors.THEME_COLOR }}>
                <SafeAreaView>
                    <Header showBackButton backAction={() => this.props.navigation.navigate('welcome')} title=''/>
                    <ScrollView style={{ height: relativeHeight(50), padding: 20, backgroundColor: colors.APP_WHITE }}>
                        <View>
                            <Text style={{ fontSize: 25, fontWeight: '400' }}>{this.props.selectedNews.title}</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#f00' }}>{this.props.selectedNews.author}</Text>
                            <Text style={{ fontSize: 12, fontWeight: '100', color: '#aaa' }}>{this.props.selectedNews.createdAt}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <TouchableOpacity onPress={this.updateNewsAction}>
                                <Text style={{ color: '#fa0', fontWeight: '100', fontSize: 12 }}>Edit </Text>
                            </TouchableOpacity>
                            <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 12 }}> | </Text>
                            <TouchableOpacity onPress={this.deleteNewsAction}>
                                <Text style={{ color: '#f00', fontWeight: '100', fontSize: 12 }}> Delete</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 300, paddingTop: 0, paddingBottom: 10, marginLeft: -20, marginRight: -20, justifyContent: 'center', alignItems: 'center' }}>
                            {this.props.fetchImagesLoading && this.renderActivityLoading()}
                            {!this.props.fetchImagesLoading &&  
                            <FlatList
                                style={{ height: '100%' }}
                                data={this.props.imageList}
                                renderItem={({ item, index }) => <SingleImage item={item} />}
                                keyExtractor={(item, index)=> `${index}`}
                                ItemSeparatorComponent={this.renderSeparator}
                                horizontal
                            />}
                        </View>
                        
                        <View>
                            <Text style={{  fontWeight: '300', color: '#000' }}>
                                {this.props.selectedNews.body}
                            </Text>
                        </View>

                        <View style={{ height: 0.5, backgroundColor: '#aaa', marginTop: 10, marginBottom: 10 }} />
                        <View>
                            <Text style={{ fontWeight: 'bold' }}>Comments</Text>
                        </View>

                        <View>
                            <FlatList
                                data={this.props.commentList}
                                renderItem={
                                    ({ item, index }) => 
                                    <CommentCell 
                                        item={item} 
                                        index={index} 
                                        editAction={() => this.updateCommentAction(item)}
                                    />
                                }
                                keyExtractor={(item, index)=> item.id}
                                ItemSeparatorComponent={this.renderVerticalSeparator}
                            />
                        </View>

                        <View style={{ height: 100 }} />
                    </ScrollView>
                    <View style={styles.commentContainer}>
                        <TouchableWithoutFeedback 
                            onPress={() => this.toggleCreateCommentVisibility(this.state.isCreateCommentVisible)}
                        >
                            <Text style={styles.commentText}>Comment</Text>
                        </TouchableWithoutFeedback>
                    </View>

                    { this.state.isCreateCommentVisible &&
                        <CreateComment 
                            getInput={(data) => this.props.getCommentInputData(data)}
                            comment={this.props.comment}
                            name={this.props.name}
                            onDismiss={() => this.toggleCreateCommentVisibility(this.state.isCreateCommentVisible)}
                            okButtonAction={() => this.submitComment()}
                            loading={this.props.postCommentLoading}
                        />
                    }

                    { this.state.isEditCommentVisible &&
                        <EditComment 
                            getInput={(data) => this.props.getCommentInputData(data)}
                            onDismiss={() => this.toggleUpdateCommentVisibility(this.state.isEditCommentVisible)}
                            comment={this.props.comment}
                            name={this.props.name}
                            okButtonAction={() => this.updateAComment()}
                            loading={this.props.updateCommentLoading}
                        />
                    }
                </SafeAreaView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    commentContainer: { 
        position: 'absolute', 
        bottom: 50, 
        width: 100, 
        height: 40, 
        backgroundColor: '#f0f', 
        justifyContent: 'center', 
        alignItems: 'center', 
        alignSelf: 'center',
        borderRadius: 20,
        shadowColor: '#000000',
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: { width: 0, height: 3 },
        elevation: 3,
    },
    commentText: {
        color: colors.APP_WHITE,
        fontSize: 15,
        fontWeight: 'bold'
    }
});

const mapStateToProps = ({ news, comments }) => {
    const { selectedNews } = news;
    const { commentList, fetchCommentsLoading, 
        current_page, fetchImagesLoading, imageList, 
        name, comment, postCommentLoading, postCommentResponse, postCommentHasError,
        selectedComment, updateCommentLoading, updateCommentResponse, updateCommentHasError  
    } = comments;

    return { selectedNews, commentList, fetchCommentsLoading,
        current_page, fetchImagesLoading, imageList, 
        name, comment, postCommentLoading, postCommentResponse, postCommentHasError,
        selectedComment, updateCommentLoading, updateCommentResponse, updateCommentHasError 
    };
};

const mapActionCreators = {
    fetchAllComments,
    fetchAllImages,
    getCommentInputData,
    postComment,
    resetCommentFormData,
    selectComment,
    editComment
};

export default connect(mapStateToProps, mapActionCreators)(SingleNewsItem);