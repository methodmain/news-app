export const TEXT_COLOR = '#484848';
export const APP_BLACK = '#000000';
export const APP_GREEN = '#21be87';
export const APP_WHITE = '#ffffff';
export const APP_RED = '#f00';
export const THEME_COLOR = '#21be87';