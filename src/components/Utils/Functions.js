import { Dimensions } from 'react-native';

const relativeHeight = height => {
    const screenHeight = Dimensions.get('window').height;
    
    const elemHeight = parseFloat(screenHeight - height);
    return elemHeight;
  };

export {
    relativeHeight
};
  