import React from 'react';
import {View, Text, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import * as colors from './Colors';

const Header = props => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" />
            <Text style={styles.headerTitle}>{props.title}</Text>
            {props.showBackButton && 
            <TouchableOpacity 
                onPress={() => props.backAction()} 
                style={styles.backButton}
            >
                <Text style={{fontWeight:"bold",fontSize:30,color:"#fff"}}>{'<'}</Text> 
            </TouchableOpacity> }
        </View>
    )
}

const styles = StyleSheet.create({
    container: { 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        paddingBottom:0, 
        height: 50,
        backgroundColor: colors.THEME_COLOR
    },
    headerTitle: {
        position: 'absolute', 
        marginTop: 20, 
        textAlign: 'center', 
        backgroundColor: 'transparent',
        width: '100%',
        height: '100%', 
        fontSize: 14, 
        fontWeight: '400', 
        color: '#000',
        justifyContent: 'center'
    },
    backButton: { 
        paddingTop: 10, 
        paddingLeft: 10,
        width:50, 
        height: '100%' 
    }
});

export default Header;