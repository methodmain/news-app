import axios from 'axios';
import { isJson } from './helper';
import {
    GET_COMMENTS_INIT,
    GET_COMMENTS_SUCCESS,
    GET_COMMENTS_FAILED,

    UPDATE_COMMENT_INIT,
    UPDATE_COMMENT_SUCCESS,
    UPDATE_COMMENT_FAILED,

    GET_IMAGES_INIT,
    GET_IMAGES_SUCCESS,
    GET_IMAGES_FAILED,

    COMMENT_GET_INPUT,
    POST_COMMENT_INIT,
    POST_COMMENT_SUCCESS,
    POST_COMMENT_FAILED,

    RESET_COMMENT_FORM,
    SELECT_COMMENT
    
} from './type';

import { BASE_URL, ALL_NEWS } from '../endpoints';

const QUANTITY_PER_PAGE = 10;

export const getCommentInputData = ({ prop, value }) => {
    return {
      type: COMMENT_GET_INPUT,
      payload: { prop, value }
    };
  };

export const fetchAllComments = (newsId) => async dispatch => {
    console.log('Fetch comments...');
    try {
      dispatch({ type: GET_COMMENTS_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}/${newsId}/comments?page=1&limit=${QUANTITY_PER_PAGE}`;
  
      let { data } = await axios.get(url)

      if (isJson(data)) {
          dispatch({ type: GET_COMMENTS_SUCCESS, payload: data });
      } else {
        const responseData = { hasError: true, errors: { title: 'Unable to fetch comments', message: data } };
          dispatch({ type: GET_COMMENTS_FAILED, payload: responseData });
      }

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to fetch comments', message: 'Something went wrong' } };
      dispatch({ type: GET_COMMENTS_FAILED, payload: data });
    }
};

export const fetchAllImages = (newsId) => async dispatch => {
    try {
      dispatch({ type: GET_IMAGES_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}/${newsId}/images?page=1&limit=${QUANTITY_PER_PAGE}`;
  
      let { data } = await axios.get(url)

      if (isJson(data)) {
        const dataLength = Object.keys(data).length;
        if (dataLength > 0) {
            dispatch({ type: GET_IMAGES_SUCCESS, payload: data });
        } else {
            //set default image if image list is empty
            dispatch({ type: GET_IMAGES_SUCCESS, payload: [{ id: 0, image: 'https://via.placeholder.com/300'}] });
        }
          
      } else {
        const responseData = { hasError: true, errors: { title: 'Unable to fetch images', message: data } };
          dispatch({ type: GET_IMAGES_FAILED, payload: responseData });
      }

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to fetch images', message: 'Something went wrong' } };
      dispatch({ type: GET_IMAGES_FAILED, payload: data });
    }
};

export const postComment = (name, comment, newsId) => async dispatch => {
    try {
      dispatch({ type: POST_COMMENT_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}/${newsId}/comments`;
      const payload = { 
          newsId,
          name,
          avatar: 'https://picsum.photos/200', //default avatar
          comment
      };
  
      let { data } = await axios.post(url, payload, {});
  
      dispatch({ type: POST_COMMENT_SUCCESS, payload: data });

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to post comment', message: 'Something went wrong' } };
      dispatch({ type: POST_COMMENT_FAILED, payload: data });
    }
  };

export const resetCommentFormData = () => dispatch => {
    dispatch({ type: RESET_COMMENT_FORM });
}

export const selectComment = (commentItem) => dispatch => {
    dispatch({ type: SELECT_COMMENT, payload: commentItem });
}

export const editComment = (name, comment, newsId, commentId) => async dispatch => {
    try {
      dispatch({ type: UPDATE_COMMENT_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}/${newsId}/comments/${commentId}`;
      const payload = { 
          name,
          comment
      };
  
      let { data } = await axios.put(url, payload, {});
  
      dispatch({ type: UPDATE_COMMENT_SUCCESS, payload: data });

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to update comment', message: 'Something went wrong' } };
      dispatch({ type: UPDATE_COMMENT_FAILED, payload: data });
    }
};