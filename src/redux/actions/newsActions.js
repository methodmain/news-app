import axios from 'axios';
import { isJson } from './helper';
import {
    GET_NEWS_INIT,
    GET_NEWS_FAILED,
    GET_NEWS_SUCCESS,

    REFRESH_NEWS_INIT,
    REFRESH_NEWS_SUCCESS,
    REFRESH_NEWS_FAILED,

    UPDATE_NEWS_INIT,
    UPDATE_NEWS_SUCCESS,
    UPDATE_NEWS_FAILED,

    SELECT_NEWS
} from './type';

import { BASE_URL, ALL_NEWS } from '../endpoints';

const QUANTITY_PER_PAGE = 10;

export const fetchAllNews = () => async dispatch => {
    console.log('Fetch news...');
    try {
      dispatch({ type: GET_NEWS_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}?page=1&limit=${QUANTITY_PER_PAGE}`;
  
      let { data } = await axios.get(url)

      if (isJson(data)) {
          dispatch({ type: GET_NEWS_SUCCESS, payload: data });
      } else {
        const responseData = { hasError: true, errors: { title: 'Unable to fetch news', message: data } };
          dispatch({ type: GET_NEWS_FAILED, payload: responseData });
      }

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Connection Failed', message: 'Check your internet connection.' } };
      dispatch({ type: GET_NEWS_FAILED, payload: data });
    }
};

export const refreshNewsList = () => async dispatch => {
    console.log('Refreshing list...');
    try {
      dispatch({ type: REFRESH_NEWS_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}?page=1&limit=${QUANTITY_PER_PAGE}`;
  
      let { data } = await axios.get(url)

      if (isJson(data)) {
          dispatch({ type: REFRESH_NEWS_SUCCESS, payload: data });
      } else {
        const responseData = { hasError: true, errors: { title: 'Unable to refresh news', message: data } };
          dispatch({ type: REFRESH_NEWS_FAILED, payload: responseData });
      }

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to refresh news', message: 'Something went wrong...' } };
      dispatch({ type: REFRESH_NEWS_FAILED, payload: data });
    }
};


export const loadMoreNewsList = (page) => async dispatch => {
    try {
      console.log('Page to fetch: ', page)
      dispatch({ type: UPDATE_NEWS_INIT });
  
      const url = `${BASE_URL}${ALL_NEWS}?page=${page}&limit=${QUANTITY_PER_PAGE}`;
  
      let { data } = await axios.get(url)

      if (isJson(data)) {
          const dataLength = Object.keys(data).length;
          const payload = {
              data,
              newPage: (dataLength > 0)? page : page - 1  //if data is empty, set current page as previous page
          }
          dispatch({ type: UPDATE_NEWS_SUCCESS, payload });
      } else {
        const responseData = { hasError: true, errors: { title: 'Unable to load more news', message: data } };
          dispatch({ type: UPDATE_NEWS_FAILED, payload: responseData });
      }

    } catch (e) {
      const data = { hasError: true, errors: { title: 'Unable to load more news', message: 'Something went wrong...' } };
      dispatch({ type: UPDATE_NEWS_FAILED, payload: data });
    }
};

export const selectNews = (newsItem) => dispatch => {
    dispatch({ type: SELECT_NEWS, payload: newsItem });
}

