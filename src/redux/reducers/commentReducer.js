import {

    GET_COMMENTS_INIT,
    GET_COMMENTS_SUCCESS,
    GET_COMMENTS_FAILED,

    UPDATE_COMMENT_INIT,
    UPDATE_COMMENT_SUCCESS,
    UPDATE_COMMENT_FAILED,

    GET_IMAGES_INIT,
    GET_IMAGES_SUCCESS,
    GET_IMAGES_FAILED,

    COMMENT_GET_INPUT,
    POST_COMMENT_INIT,
    POST_COMMENT_SUCCESS,
    POST_COMMENT_FAILED,

    RESET_COMMENT_FORM,
    SELECT_COMMENT

  } from '../actions/type';
  
  const INITIAL_STATE = {
    //fetch news
    commentList: [],
    fetchCommentsLoading: false,
    fetchCommentsHasError: false,
    fetchNewsResponse: '',
  
    updateCommentsLoading: false,
    updateCommentsResponse: '',
    current_page: 1,

    fetchImagesLoading: false,
    imageList: [],
    
    name: '',
    comment: '',
    postCommentLoading: false,
    postCommentResponse: [],
    postCommentHasError: false,

    selectedComment: [],

    updateCommentLoading: false,
    updateCommentResponse: [],
    updateCommentHasError: false,
  };
    
  export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case COMMENT_GET_INPUT:
            return { ...state, [action.payload.prop]: action.payload.value };

        case GET_COMMENTS_INIT:
            return { ...state, fetchCommentsLoading: true };
        case GET_COMMENTS_FAILED:
            return { ...state, fetchCommentsLoading: false, fetchNewsResponse: action.payload };
        case GET_COMMENTS_SUCCESS:
            return { ...state, fetchCommentsLoading: false, commentList: action.payload, current_page: 1 };

        case GET_IMAGES_INIT:
            return { ...state, fetchImagesLoading: true, imageList: [] };
        case GET_IMAGES_SUCCESS:
            return { ...state, fetchImagesLoading: false, imageList: action.payload };
        case GET_IMAGES_FAILED:
            return { ...state, fetchImagesLoading: false, imageList: [] };

        case POST_COMMENT_INIT:
            return { ...state, postCommentLoading: true };
        case POST_COMMENT_SUCCESS:
            return { ...state, postCommentLoading: false, postCommentResponse: action.payload, postCommentHasError: false, name: '', comment: '' };
        case POST_COMMENT_FAILED:
            return { ...state, postCommentLoading: false, postCommentResponse: action.payload, postCommentHasError: true };

        case RESET_COMMENT_FORM:
            return { ...state, name: '', comment: '', postCommentHasError: false };

        case SELECT_COMMENT:
            return { ...state, selectedComment: action.payload, name: action.payload.name, comment: action.payload.comment };

        case UPDATE_COMMENT_INIT:
            return { ...state, updateCommentLoading: true };
        case UPDATE_COMMENT_SUCCESS:
            return { ...state, updateCommentLoading: false, updateCommentResponse: action.payload, updateCommentHasError: false, name: '', comment: '' };
        case UPDATE_COMMENT_FAILED:
            return { ...state, updateCommentLoading: false, updateCommentResponse: action.payload, updateCommentHasError: true };
    
        default:
            return state;
    }
  }
    