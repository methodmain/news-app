import { persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import news from './newsReducer';
import comments from './commentReducer';

const config = {
  key: 'primary',
  storage: storage,
  blacklist: []
 }

 const reducers = {
   news,
   comments
 }

export default persistCombineReducers(config, reducers);
