import {
  GET_NEWS_INIT,
  GET_NEWS_FAILED,
  GET_NEWS_SUCCESS,

  REFRESH_NEWS_INIT,
  REFRESH_NEWS_SUCCESS,
  REFRESH_NEWS_FAILED,

  UPDATE_NEWS_INIT,
  UPDATE_NEWS_SUCCESS,
  UPDATE_NEWS_FAILED,

  SELECT_NEWS
} from '../actions/type';

const INITIAL_STATE = {
  //fetch news
  newsList: [],
  fetchNewsLoading: false,
  fetchNewsHasError: false,
  fetchNewsResponse: '',
  
  refreshNewsLoading: false,
  refreshNewsResponse: '',

  updateNewsLoading: false,
  updateNewsResponse: '',
  current_page: 1,

  selectedNews : {
    id: 0,
    author: '',
    title: '',
    url: '',
    body: '',
    createdAt: ''
  }

};
  
export default function (state = INITIAL_STATE, action) {
  switch (action.type) {

    case GET_NEWS_INIT:
      return { ...state, fetchNewsLoading: true };
    case GET_NEWS_FAILED:
      return { ...state, fetchNewsLoading: false, fetchNewsResponse: action.payload };
    case GET_NEWS_SUCCESS:
      return { ...state, fetchNewsLoading: false, newsList: action.payload, current_page: 1 };

    case REFRESH_NEWS_INIT:
      return { ...state, refreshNewsLoading: true };
    case REFRESH_NEWS_SUCCESS:
      return { ...state, refreshNewsLoading: false, refreshNewsResponse: action.payload, current_page: 1 };
    case REFRESH_NEWS_FAILED:
      return { ...state, refreshNewsLoading: false, newsList: action.payload };

    case UPDATE_NEWS_INIT:
      return { ...state, updateNewsLoading: true };
    case UPDATE_NEWS_SUCCESS:
      return { ...state, updateNewsLoading: false, newsList: [...state.newsList, ...action.payload.data], current_page: action.payload.newPage };
    case UPDATE_NEWS_FAILED:
      return { ...state, updateNewsLoading: false, updateNewsResponse: action.payload };
    
    case SELECT_NEWS:
      return { ...state, selectedNews: action.payload };

    default:
      return state;
  }
}
  