import { createStore, compose, applyMiddleware } from 'redux';
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk';
//import AsyncStorage from '@react-native-community/async-storage';
import { persistStore } from 'redux-persist';
import reducers from '../reducers';

const config = {
  key: 'primary',
  storage
 }

const store = createStore(
  reducers,
  {},
  compose(
    applyMiddleware(thunk),
  )
);

let persistor = persistStore(
  store,
  null,
  () => {
    store.getState()
  }
)

export default store;
