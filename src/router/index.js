import {
    createSwitchNavigator,
    createAppContainer,
  } from "react-navigation";

  import Feed from '../components/Home/Feeds'
  import SingleNewsItem from '../components/Home/SingleNewsItem'
  import CreateNewsForm from '../components/Home/CreateNewsForm'
  import UpdateNewsForm from '../components/Home/UpdateNewsForm'
  import CreateComment from '../components/Home/CreateComment'
  import EditComment from '../components/Home/EditComment'

  const AppContainer = createSwitchNavigator({
    welcome: { screen: Feed },
    news: { screen: SingleNewsItem },
    create: { screen: CreateNewsForm },
    updateNews: { screen: UpdateNewsForm },
    createComment: { screen: CreateComment },
    updateComment: { screen: EditComment },
  },
  {
    initialRouteName: 'welcome',
  });

  export default createAppContainer(AppContainer);